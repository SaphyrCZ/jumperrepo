﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class JumperController : MonoBehaviour
{
    public float SwipeDeadZone;
    public float SwipeLenght;
    private int Level;

    public int Lives = 1;
    public int Points = 0;
    private int StartPoints;


    private bool CanRun = false;
    private bool Touched = false;
    private static bool IsInputEnabled = true;
    



    

    private Vector2 StartTouchPos, EndTouchPos;
  //charakter  
    [SerializeField] private int RunSpeed;
    [SerializeField] private int ForwardJumpForce;
    [SerializeField] private int ForwardJumpSpeed;
    [SerializeField] private int HighJumpForce;
    [SerializeField] private int HighJumpSpeed;
    [SerializeField] private int LowJumpForce;
    [SerializeField] private int LowJumpSpeed;
    [SerializeField] private int ShortJumpForce;
    [SerializeField] private int ShortJumpSpeed;

    [SerializeField] private LayerMask GroundLayer;
    [SerializeField] private Transform FlameFart;

    [SerializeField] private Text LivesText;
    [SerializeField] private Text PointsText;




    //audio
    [SerializeField] private AudioSource FootSteps;
    [SerializeField] private AudioSource DeathSound;
    [SerializeField] private AudioSource JumpSound;


    private Animator Anim;
    private Rigidbody2D Rb;
    private Collider2D Coll;
    private SpriteRenderer Sprt;

   
    
    

    private enum States {Idle, Runnig, FJump,HJump,LJump,ShJump,Crash,Fall };
    private States JumperState = States.Idle;

    
    

    void Start()
    {
        Anim = GetComponent<Animator>();
        Rb = GetComponent<Rigidbody2D>();
        Coll = GetComponent<Collider2D>();
        Sprt = GetComponent<SpriteRenderer>();
        IsInputEnabled = true;

        // nacte nastaveni dotyku
        OptionsData data = SaveSystem.LoadOptions();

        SwipeDeadZone = data.DeadZone;
        SwipeLenght = data.Lenght;

        //ulozi aktualni level
        Level = SceneManager.GetActiveScene().buildIndex;
        PlayerPrefs.SetInt("Level", Level);

        // nacte data hrace
        string path = Path.Combine(Application.persistentDataPath + "jumper.fck");

        if (File.Exists(path))
        {

        PlayerData SavedData = SaveSystem.LoadJumper();
        Lives = SavedData.Lives;
        Points = SavedData.Points;
        StartPoints = Points;
        }

        // zobrazi zivoty a body
        LivesText.text = Lives.ToString();
        PointsText.text = Points.ToString();
        

    }

   
    void Update()
    {
        if (IsInputEnabled)
        {

            SwipeManager();
            Run();
            UpdateAnimation();


        }
    }





    private void OnCollisionEnter2D(Collision2D collision)
    {
        //naraz na prekazku
        if (collision.gameObject.tag == "Obstacle")
        {
            CanRun = false;
            Rb.velocity = new Vector2(0, 0);
            Rb.gravityScale = 0;
            FlameFart.GetComponent<ParticleSystem>().enableEmission = false;
            JumpSound.Stop();
            DeathSound.Play();
            Lives--;
            LivesText.text = Lives.ToString();
            Points = StartPoints;
            SaveSystem.SaveJumper(this);

            
            JumperState = States.Crash;
            

        }

        // konec levelu
        if (collision.gameObject.tag == "LevelEnd")
        {
            Rb.velocity = new Vector2(0, 0);
            Rb.gravityScale = 0;
            CanRun = false;
            
            JumperState = States.Idle;
            Lives += 2;
            LivesText.text = Lives.ToString();
            SaveSystem.SaveJumper(this);
           
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "PointGate")
        {
            Points += 10;
            PointsText.text = Points.ToString();
            print(Points);
        }
    }






    private void Run()
    {
        float TurnButtonPressed = Input.GetAxis("Jump");
        if ((Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Ended) || TurnButtonPressed > 0f)
        { 

            if (CanRun == false)
            {
                CanRun = true;
                
                
            }
            else
            {
                if (Coll.IsTouchingLayers(GroundLayer))
                {
                    Rb.transform.localScale = new Vector3(Rb.transform.localScale.x * -1, 1, 1);

                    RunSpeed = RunSpeed * (-1);
                    ForwardJumpSpeed = ForwardJumpSpeed * (-1);
                    HighJumpSpeed = HighJumpSpeed * (-1);
                    LowJumpSpeed = LowJumpSpeed * (-1);
                    ShortJumpSpeed = ShortJumpSpeed * (-1);

                }
            }

           

        }

        if (CanRun == true)
        {
            Rb.velocity = new Vector2(RunSpeed, Rb.velocity.y);

            if (Input.GetButtonDown("HighJump") && Coll.IsTouchingLayers(GroundLayer))
            {
                HighJump();                
            }

            if (Input.GetButtonDown("LowJump") && Coll.IsTouchingLayers(GroundLayer))
            {
                LowJump();
            }

            if (Input.GetButtonDown("ForwardJump") && Coll.IsTouchingLayers(GroundLayer))
            {
                ForwardJump();
            }

            if (Input.GetButtonDown("ShortJump") && Coll.IsTouchingLayers(GroundLayer))
            {
                ShortJump();
            }


        }

    }

    private void UpdateAnimation()
    {
        if (JumperState == States.FJump)
        {
            if (Rb.velocity.y < .5f)
            {
                JumperState = States.Fall;
            }
        }
        else if (JumperState == States.HJump)
        {
            if (Rb.velocity.y < .5f)
            {
                JumperState = States.Fall;
            }

        }
        else if (JumperState == States.LJump)
        {
            if (Rb.velocity.y < .5f)
            {
                JumperState = States.Fall;
            }

        }
        else if (JumperState == States.ShJump)
        {
            if (Rb.velocity.y < .5f)
            {
                JumperState = States.Fall;
                
            }

        }
        else if (JumperState == States.Fall)
        {
            FlameFart.GetComponent<ParticleSystem>().enableEmission = false;
            JumpSound.Stop();

            if (Coll.IsTouchingLayers(GroundLayer))
            {
                CanRun = true;
                JumperState = States.Idle;

            }

        }
        else if ((Rb.velocity.x > .1f || Rb.velocity.x < (-.1f)) && (JumperState != States.FJump || JumperState != States.HJump || JumperState != States.ShJump || JumperState != States.LJump))
        {
            JumperState = States.Runnig;

        }
        else
        {
            // JumperState = States.Idle;
        }


        Anim.SetInteger("State", (int)JumperState);
    }

    private void SwipeManager()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began )
        {

            StartTouchPos = Input.GetTouch(0).position;

        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved && Touched == false)
        {
            EndTouchPos = Input.GetTouch(0).position;



            if ((EndTouchPos.x < (StartTouchPos.x - SwipeDeadZone)) && (Coll.IsTouchingLayers(GroundLayer) && CanRun == true ))
            {
                
                if(EndTouchPos.x <= (StartTouchPos.x - SwipeDeadZone - SwipeLenght))
                {

                    CanRun = false;
                    ShortJump();
                    Touched = true;
                
                }

                
            }

            else if ((EndTouchPos.x > (StartTouchPos.x + SwipeDeadZone)) && Coll.IsTouchingLayers(GroundLayer) && CanRun == true)
            {

                if (EndTouchPos.x >= (StartTouchPos.x + SwipeDeadZone + SwipeLenght))
                {
                    CanRun = false;
                    ForwardJump();
                    Touched = true;
                }
            }

            else if ((EndTouchPos.y < (StartTouchPos.y - SwipeDeadZone)) && Coll.IsTouchingLayers(GroundLayer) && CanRun == true)
            {
                if (EndTouchPos.y <= (StartTouchPos.y - SwipeDeadZone - SwipeLenght))
                {
                    CanRun = false;
                    LowJump();
                    Touched = true;
                }
            }

            else if ((EndTouchPos.y > (StartTouchPos.y + SwipeDeadZone)) && Coll.IsTouchingLayers(GroundLayer) && CanRun == true)
            {
                if (EndTouchPos.y >= (StartTouchPos.y + SwipeDeadZone + SwipeLenght))
                {
                    CanRun = false;
                    HighJump();
                    Touched = true;
                }

            }
             
        }


        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            Touched = false;
        }


    }


    private void ForwardJump()
    {
        Rb.velocity = new Vector2(ForwardJumpSpeed, ForwardJumpForce);
        JumperState = States.FJump;
        FlameFart.GetComponent<ParticleSystem>().enableEmission = true;
        JumpSound.Play();

    }

    private void HighJump()
    {
        Rb.velocity = new Vector2(HighJumpSpeed, HighJumpForce);
        JumperState = States.HJump;
        FlameFart.GetComponent<ParticleSystem>().enableEmission = true;
        JumpSound.Play();
    }

    private void LowJump()
    {
        Rb.velocity = new Vector2(LowJumpSpeed, LowJumpForce);
        JumperState = States.LJump;
        FlameFart.GetComponent<ParticleSystem>().enableEmission = true;
        JumpSound.Play();
    }

    private void ShortJump()
    {
        Rb.velocity = new Vector2(ShortJumpSpeed, ShortJumpForce);
        JumperState = States.ShJump;
        FlameFart.GetComponent<ParticleSystem>().enableEmission = true;
        JumpSound.Play();
    }


    
    private void FootStepSound()
    {
        if (Coll.IsTouchingLayers(GroundLayer))
        {

            FootSteps.Play();
        }
    }


    private IEnumerator Death()
    {

       
        yield return new WaitForSeconds(1.5f);

        if (Lives <= 0)
        {
            PlayerPrefs.DeleteAll();
            SceneManager.LoadScene("GameOver");
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
        }



    }

   private void DisableInput()
    {
        IsInputEnabled = false;
    }

     
   

}
