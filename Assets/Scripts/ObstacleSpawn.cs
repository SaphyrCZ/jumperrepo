﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;





public class ObstacleSpawn : MonoBehaviour
{



    private int set;
    public int SpawnNumber;
    public int SpawnerID;
   

    private SpriteRenderer Ren;
   
    [SerializeField] private GameObject[] Obstacle;

   


    void Start()
    {
        Ren = GetComponent<SpriteRenderer>();
        Ren.enabled = false;

        set = PlayerPrefs.GetInt("Set" + SpawnerID);
        
        if(set == 0)
        {
            SpawnNumber = Random.Range(0, Obstacle.Length);
            Instantiate(Obstacle[SpawnNumber], transform.position, Quaternion.identity);
            PlayerPrefs.SetInt("ObstacleNum" + SpawnerID, SpawnNumber);
            set = 1;
            PlayerPrefs.SetInt("Set" + SpawnerID, set);
        }
        else
        {
            SpawnNumber = PlayerPrefs.GetInt("ObstacleNum" + SpawnerID);
            Instantiate(Obstacle[SpawnNumber], transform.position, Quaternion.identity);
            
        }
            
        
    }


    
}
