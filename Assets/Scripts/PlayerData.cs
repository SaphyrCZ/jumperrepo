﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData 
{
    public int Lives;
    public int Points;

    public PlayerData(JumperController Jumper)
    {
        Lives = Jumper.Lives;
        Points = Jumper.Points;
    }
}
