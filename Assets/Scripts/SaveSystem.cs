﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public  static class SaveSystem 
{

    //uloží data nastaveni
    public static void SaveOptions(OptionsMenu Options)
    {
       
        BinaryFormatter Formatter = new BinaryFormatter();

        string path = Path.Combine(Application.persistentDataPath + "options.set");
        FileStream stream = new FileStream(path, FileMode.Create);

        OptionsData data = new OptionsData(Options);

        Formatter.Serialize(stream, data);
        stream.Close();

    }

    //nahraje data nastaveni
    public static OptionsData LoadOptions()
    {
        string path = Path.Combine(Application.persistentDataPath + "options.set");
        if (File.Exists(path))
        {
            BinaryFormatter Formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            OptionsData data = Formatter.Deserialize(stream) as OptionsData;
            stream.Close();

            return data;
                
        }
        else
        {
            return null;
        }

    }

    //ulozi data hrace
    public static void SaveJumper(JumperController Jumper)
    {
        BinaryFormatter Formatter = new BinaryFormatter();

        string path = Path.Combine(Application.persistentDataPath + "jumper.fck");
        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData(Jumper);

        

        Formatter.Serialize(stream, data);
        stream.Close();

    }

    public static PlayerData LoadJumper()
    {
        string path = Path.Combine(Application.persistentDataPath + "jumper.fck");

        if (File.Exists(path))
        {
            BinaryFormatter Formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = Formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;


        }
        else
        {
            return null;
        }


    }

}
