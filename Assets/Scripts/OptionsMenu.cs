﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionsMenu : MonoBehaviour
{
    public float SavedZone;
    public float SavedLenght;
    public float DefaultZone = 25;
    public float DefaultLenght = 25;


    [SerializeField] private Slider SliderZone;
    [SerializeField] private Slider SliderLenght;

    private void Start()
    {
        string path = Path.Combine(Application.persistentDataPath + "options.set");

        DefaultZone = Screen.currentResolution.width / 81.92f;
        DefaultLenght = Screen.currentResolution.width / 81.92f;

        if (File.Exists(path))
        {
            OptionsData data = SaveSystem.LoadOptions();

            SavedZone = data.DeadZone;
            SavedLenght = data.Lenght;

        }
        else
        {
            SavedZone = DefaultZone;
            SavedLenght = DefaultLenght;



        }
       
        SliderZone.value = SavedZone;
        SliderLenght.value = SavedLenght;


    }

    public void SetSwipeDeadZone(float DeadZone)
    {

        PlayerPrefs.SetFloat("SwipeDeadZone", DeadZone);
        SavedZone = DeadZone;
      
    }

    public void SetSwipeLenght(float Lenght)
    {
        PlayerPrefs.SetFloat("SwipeLenght", Lenght);
        SavedLenght = Lenght;

    }

    public void DefaultValues()
    {
        SliderZone.value = DefaultZone;
        SliderLenght.value = DefaultLenght;
        SavedZone = DefaultZone;
        SavedLenght = DefaultLenght; 
        
        SaveSystem.SaveOptions(this);

        print("zona: " + DefaultZone);
        print("delka: " + DefaultLenght);
    }

    public void BackToMenu() 
    {
        
        
        SaveSystem.SaveOptions(this);
        SceneManager.LoadScene("MainMenu");

    
    
    }




}
