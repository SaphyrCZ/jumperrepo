﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    

    [SerializeField] public Button ResumeButton;


   

    public void PlayGame()
    {
        string path = Path.Combine(Application.persistentDataPath + "jumper.fck");
        if (File.Exists(path))
        {
            try 
            { 
                File.Delete(path);
            }
            catch
            {
                
            }
        }

        PlayerPrefs.DeleteAll();   
        SceneManager.LoadScene(1);

    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OpenHelp()
    {
        SceneManager.LoadScene("Help");
    }

    public void OpenOptions()
    {
        SceneManager.LoadScene("Options");
    }

    public void ResumeGame()
    {
        int Level = PlayerPrefs.GetInt("Level");
        SceneManager.LoadScene(Level);

    }
}
